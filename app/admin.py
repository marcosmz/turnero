from django.contrib import admin
from . import models

# Register your models here.

class CargoAdmin(admin.ModelAdmin):
    list_display = ["estado_cargo","cargo_descripcion"]
    list_filter = ["cargo_descripcion"]
    list_editable = ["cargo_descripcion"]
    search_fields = ["cargo_descripcion"]


class EmpresaAdmin(admin.ModelAdmin):
    list_display = ["empresa_id","nombre","nro_telefono","direccion"]
    list_editable = ["nombre","direccion","nro_telefono"]
    

class SexoAdmin(admin.ModelAdmin):
    list_display = ["estado_sexo","sexo_descripcion",]
    list_filter = ["sexo_descripcion"]
    list_editable = ["sexo_descripcion"]
    search_fields = ["sexo_descripcion"]


class TipopersonaAdmin(admin.ModelAdmin):  
    list_display = ["tipo_persona_id","tipo_persona_descripcion"]
    list_filter = ["tipo_persona_descripcion"]
    list_editable = ["tipo_persona_descripcion"]
    search_fields = ["tipo_persona_descripcion"]


class PersonaAdmin(admin.ModelAdmin):
    list_display = ["estado_persona","nro_documento","ruc","nombre","apellido","nro_telefono","direccion"]
    list_filter = ["nro_documento"]
    list_editable = ["direccion"]
    search_fields = ["direccion"]

class FuncionarioAdmin(admin.ModelAdmin):
    list_display = ["funcionario_id","persona","empresa","estado_funcionario"]
    list_filter = ["persona"]
    list_editable = ["persona"]
    search_fields = ["persona"]


class CargofuncionarioAdmin(admin.ModelAdmin):
    list_display = ["funcionario","cargo","fecha_inicio_cargo","fecha_fin_cargo"]
    list_editable = ["fecha_inicio_cargo","fecha_fin_cargo"]
    search_fields = ["funcionario"]


class ServiciosAdmin(admin.ModelAdmin):
    list_display = ["servicio_id","servicio_descripcion","estado_servicio"]
    list_filter = ["servicio_descripcion"]
    list_editable = ["servicio_descripcion"]
    search_fields = ["servicio_descripcion"]


class EstadoAdmin(admin.ModelAdmin):
    list_display = ["estado_id","estado_descripcion"]
    list_filter = ["estado_descripcion"]
    list_editable = ["estado_descripcion"]
    search_fields = ["estado_descripcion"]


class TurnoAdmin(admin.ModelAdmin):
    list_display = ["turno_id","numero_turno","fecha_turno","hora_turno","estado"]
    list_filter = ["numero_turno"]
    list_editable = ["estado","fecha_turno","hora_turno"]
    search_fields = ["numero_turno"]


class PrioridadAdmin(admin.ModelAdmin):
    list_display = ["prioridad_id","prioridad_descripcion","nivel"]
    list_filter = ["prioridad_descripcion"]
    list_editable = ["prioridad_descripcion","nivel"]
    search_fields = ["prioridad_descripcion"]


class PuestoAdmin(admin.ModelAdmin):
    list_display = ["puesto_id","puesto_descripcion","servicio"]
    list_filter = ["puesto_descripcion"]
    list_editable = ["puesto_descripcion","servicio"]
    search_fields = ["puesto_descripcion"]


class FuncionariopuestoAdmin(admin.ModelAdmin):
    list_display = ["funcionario","puesto","fecha_puesto"]
    list_editable = ["fecha_puesto"]
    search_fields = ["funcionario"]


class ClienteAdmin(admin.ModelAdmin):
    list_display = ["cliente_id","persona","estado_cliente"]
    list_filter = ["persona"]
    list_editable = ["estado_cliente"]
    search_fields = ["persona"]


class ColaAdmin(admin.ModelAdmin):
    list_display = ["cola_id","cliente","servicio","prioridad","turno","turno_anterior","turno_siguiente"]
    list_editable = ["servicio","prioridad"]
    search_fields = ["cliente"]


class PermisosAdmin(admin.ModelAdmin):
    list_display = ["permiso_id","permiso_descripcion","estado_permiso"]
    list_filter = ["permiso_descripcion"]
    list_editable = ["permiso_descripcion"]
    search_fields = ["permiso_descripcion"]


class RolAdmin(admin.ModelAdmin):
    list_display = ["rol_id","rol_descripcion","estado_rol"]
    list_filter = ["rol_descripcion"]
    list_editable = ["rol_descripcion"]
    search_fields = ["rol_descripcion"]


class UsuarioAdmin(admin.ModelAdmin):
    list_display = ["usuario_id","nombre_usuario","rol","funcionario","contrasenha_usuario","estado_usuario"]
    list_filter = ["nombre_usuario"]
    list_editable = ["nombre_usuario","estado_usuario"]
    search_fields = ["nombre_usuario"]


admin.site.register(models.Cargo,CargoAdmin) 
admin.site.register(models.Empresa,EmpresaAdmin)
admin.site.register(models.Sexo,SexoAdmin)  
admin.site.register(models.Tipopersona,TipopersonaAdmin)  
admin.site.register(models.Persona,PersonaAdmin)  
admin.site.register(models.Funcionario,FuncionarioAdmin)  
admin.site.register(models.Cargofuncionario,CargofuncionarioAdmin)  
admin.site.register(models.Servicios,ServiciosAdmin)  
admin.site.register(models.Estado,EstadoAdmin)  
admin.site.register(models.Turno,TurnoAdmin)  
admin.site.register(models.Prioridad,PrioridadAdmin)  
admin.site.register(models.Puesto,PuestoAdmin)  
admin.site.register(models.Funcionariopuesto,FuncionariopuestoAdmin)  
admin.site.register(models.Cliente,ClienteAdmin)  
admin.site.register(models.Cola,ColaAdmin)  
admin.site.register(models.Permisos,PermisosAdmin)  
admin.site.register(models.Rol,RolAdmin)  
admin.site.register(models.Usuario,UsuarioAdmin)  