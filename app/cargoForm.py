from django import forms

class CargoFormulario(forms.Form):
    descripcion = forms.CharField(max_length=20, required=True)
    estado = forms.BooleanField(required=False)

    print(dir(forms))