# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


# class AuthGroup(models.Model):
#     name = models.CharField(unique=True, max_length=150)

#     class Meta:
#         # managed = False
#         db_table = 'auth_group'


# class AuthUser(models.Model):
#     password = models.CharField(max_length=128)
#     last_login = models.DateTimeField(blank=True, null=True)
#     is_superuser = models.BooleanField()
#     username = models.CharField(unique=True, max_length=150)
#     first_name = models.CharField(max_length=150)
#     last_name = models.CharField(max_length=150)
#     email = models.CharField(max_length=254)
#     is_staff = models.BooleanField()
#     is_active = models.BooleanField()
#     date_joined = models.DateTimeField()

#     class Meta:
#         # managed = False
#         db_table = 'auth_user'


# class DjangoContentType(models.Model):
#     app_label = models.CharField(max_length=100)
#     model = models.CharField(max_length=100)

#     class Meta:
#         # managed = False
#         db_table = 'django_content_type'
#         unique_together = (('app_label', 'model'),)


# class DjangoAdminLog(models.Model):
#     action_time = models.DateTimeField()
#     object_id = models.TextField(blank=True, null=True)
#     object_repr = models.CharField(max_length=200)
#     action_flag = models.SmallIntegerField()
#     change_message = models.TextField()
#     content_type = models.ForeignKey('DjangoContentType', on_delete=models.CASCADE, blank=True, null=True)
#     user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)

#     class Meta:
#         # managed = False
#         db_table = 'django_admin_log'


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        # managed = False
        db_table = 'django_migrations'


# class DjangoSession(models.Model):
#     session_key = models.CharField(primary_key=True, max_length=40)
#     session_data = models.TextField()
#     expire_date = models.DateTimeField()

#     class Meta:
#         # managed = False
#         db_table = 'django_session'


# class AuthPermission(models.Model):
#     name = models.CharField(max_length=255)
#     content_type = models.ForeignKey('DjangoContentType', on_delete=models.CASCADE)
#     codename = models.CharField(max_length=100)

#     class Meta:
#         # managed = False
#         db_table = 'auth_permission'
#         unique_together = (('content_type', 'codename'),)


# class AuthGroupPermissions(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     group = models.ForeignKey(AuthGroup, on_delete=models.CASCADE)
#     permission = models.ForeignKey('AuthPermission', on_delete=models.CASCADE)

#     class Meta:
#         # managed = False
#         db_table = 'auth_group_permissions'
#         unique_together = (('group', 'permission'),)


# class AuthUserGroups(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
#     group = models.ForeignKey(AuthGroup, on_delete=models.CASCADE)

#     class Meta:
#         # managed = False
#         db_table = 'auth_user_groups'
#         unique_together = (('user', 'group'),)


# class AuthUserUserPermissions(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
#     permission = models.ForeignKey(AuthPermission, on_delete=models.CASCADE)

#     class Meta:
#         # managed = False
#         db_table = 'auth_user_user_permissions'
#         unique_together = (('user', 'permission'),)


class Cargo(models.Model):
    cargo_id = models.AutoField(primary_key=True)
    estado_cargo = models.BooleanField()
    cargo_descripcion = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=16, null=True)

    class Meta:
        db_table = 'cargo'

    def __str__(self):
        return format(self.cargo_descripcion)


class Empresa(models.Model):
    empresa_id = models.AutoField(primary_key=True)
    nro_telefono = models.CharField(max_length=15)
    fecha_constitucion = models.DateField(blank=True, null=True)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    nombre = models.CharField(max_length=100)

    class Meta:
        db_table = 'empresa'

    def __str__(self):
        return format(self.nombre)


class Sexo(models.Model):
    sexo_id = models.AutoField(primary_key=True)
    sexo_descripcion = models.CharField(max_length=20)
    estado_sexo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)

    class Meta:
        db_table = 'sexo'

    def __str__(self):
        return format(self.sexo_descripcion)
        # return 'Sexo: {}'.format(self.sexo_descripcion)


class Tipopersona(models.Model):
    tipo_persona_id = models.AutoField(primary_key=True)
    tipo_persona_descripcion = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
        db_table = 'tipopersona'

    def __str__(self):
        return format(self.tipo_persona_descripcion)


class Persona(models.Model):
    persona_id = models.AutoField(primary_key=True)
    tipo_persona = models.ForeignKey('Tipopersona', on_delete=models.CASCADE)
    sexo = models.ForeignKey('Sexo', on_delete=models.CASCADE)
    nro_documento = models.CharField(max_length=20, blank=True, null=True)
    ruc = models.CharField(max_length=15, blank=True, null=True)
    nombre = models.CharField(max_length=100)
    nro_telefono = models.CharField(max_length=15)
    apellido = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    estado_persona = models.BooleanField()

    class Meta:
        db_table = 'persona'

    def __str__(self):
        return format(self.nro_documento)


class Funcionario(models.Model):
    funcionario_id = models.AutoField(primary_key=True)
    persona = models.ForeignKey('Persona', on_delete=models.CASCADE)
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField(blank=True, null=True)
    estado_funcionario = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
        db_table = 'funcionario'

    def __str__(self):
        return format(self.persona)



class Cargofuncionario(models.Model):
    cargo = models.OneToOneField(Cargo, on_delete=models.CASCADE, primary_key=True)
    funcionario = models.ForeignKey('Funcionario', on_delete=models.CASCADE)
    fecha_inicio_cargo = models.DateField()
    fecha_fin_cargo = models.DateField(blank=True, null=True)

    class Meta:
        db_table = 'cargofuncionario'
        unique_together = (('cargo', 'funcionario'),)

    def __str__(self):
        return format(self.funcionario)


class Servicios(models.Model):
    servicio_id = models.AutoField(primary_key=True)
    servicio_descripcion = models.CharField(max_length=20)
    estado_servicio = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
        db_table = 'servicios'

    def __str__(self):
        return format(self.servicio_descripcion)    


class Estado(models.Model):
    estado_id = models.AutoField(primary_key=True)
    estado_descripcion = models.CharField(max_length=20)

    class Meta:
        db_table = 'estado'

    def __str__(self):
        return format(self.estado_descripcion)  


class Turno(models.Model):
    turno_id = models.AutoField(primary_key=True)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)
    numero_turno = models.CharField(max_length=3)
    fecha_turno = models.DateField()
    hora_turno = models.TimeField()

    class Meta:
        db_table = 'turno'

    def __str__(self):
        return format(self.numero_turno) 


class Prioridad(models.Model):
    prioridad_id = models.AutoField(primary_key=True)
    prioridad_descripcion = models.CharField(max_length=20)
    nivel = models.CharField(max_length=20)
    usuario_modificacion = models.CharField(max_length=16)
    usuario_insercion = models.CharField(max_length=16)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField()

    class Meta:
        db_table = 'prioridad'

    def __str__(self):
        return format(self.prioridad_descripcion)    


class Puesto(models.Model):
    puesto_id = models.AutoField(primary_key=True)
    servicio = models.ForeignKey('Servicios', on_delete=models.CASCADE)
    puesto_descripcion = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
        db_table = 'puesto'

    def __str__(self):
        return format(self.puesto_descripcion)    


class Funcionariopuesto(models.Model):
    funcionario = models.OneToOneField(Funcionario, on_delete=models.CASCADE, primary_key=True)
    puesto = models.ForeignKey('Puesto', on_delete=models.CASCADE)
    fecha_puesto = models.DateTimeField() #modificado

    class Meta:
        db_table = 'funcionariopuesto'
        unique_together = (('funcionario', 'puesto'),) 

    def __str__(self):
        return format(self.funcionario)        


class Cliente(models.Model):
    cliente_id = models.AutoField(primary_key=True)
    persona = models.ForeignKey('Persona', on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    estado_cliente = models.BooleanField()

    class Meta:
        db_table = 'cliente'

    def __str__(self):
        return format(self.persona)     


class Cola(models.Model):
    cola_id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    servicio = models.ForeignKey('Servicios', on_delete=models.CASCADE)
    prioridad = models.ForeignKey('Prioridad', on_delete=models.CASCADE)
    turno_anterior = models.CharField(max_length=10)
    turno = models.ForeignKey('Turno', on_delete=models.CASCADE)
    turno_siguiente = models.CharField(max_length=10)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    usuario_modificacion = models.CharField(max_length=16)

    class Meta:
        db_table = 'cola'




class Permisos(models.Model):
    permiso_id = models.AutoField(primary_key=True)
    permiso_descripcion = models.CharField(max_length=50)
    usuario_insercion = models.CharField(max_length=16)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    estado_permiso = models.BooleanField()

    class Meta:
        db_table = 'permisos'

    def __str__(self):
        return format(self.permiso_descripcion) 


class Rol(models.Model):
    rol_id = models.AutoField(primary_key=True)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    rol_descripcion = models.CharField(max_length=20)
    estado_rol = models.BooleanField()

    class Meta:
        db_table = 'rol'

    def __str__(self):
        return format(self.rol_descripcion)     


class Rolpermiso(models.Model):
    rol = models.OneToOneField(Rol, on_delete=models.CASCADE, primary_key=True)
    permiso = models.ForeignKey(Permisos, on_delete=models.CASCADE)

    class Meta:
        db_table = 'rolpermiso'
        unique_together = (('rol', 'permiso'),)




class Usuario(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    funcionario = models.ForeignKey(Funcionario, on_delete=models.CASCADE)
    nombre_usuario = models.CharField(max_length=20)
    contrasenha_usuario = models.CharField(max_length=10)
    estado_usuario = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
        db_table = 'usuario'

    def __str__(self):
        return format(self.nombre_usuario)    