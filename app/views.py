from django.shortcuts import render
from .models import Cargo
from .cargoForm import CargoFormulario
# Create your views here.

def Inicio(request):
    form = CargoFormulario(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)
        aux1 = form_data.get("descripcion")
        aux2 = form_data.get("estado")
        obj = Cargo.objects.create(cargo_descripcion=aux1, estado_cargo=aux2)
    context = {
        "titulo":'Formulario Cargo',
        "el_form":form
    }
    return render(request,"inicio.html",context)
